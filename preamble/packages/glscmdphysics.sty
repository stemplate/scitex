\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{preamble/packages/glscmdphysics}[2023/11/19 Maths glossary commands]

\RequirePackage{preamble/packages/glscmdmaths}

% basic commands (before glossary entry definitions) ----------------------- %

% physics
\newcommand{\lambdabar}{{\mkern0.75mu\mathchar'26\mkern-9.75mu\lambda}}
\newcommand{\basepermutationoperator}[1]{\ensuremath{P_{#1}}}
\newcommand{\basecreationoperator}[1]{\ensuremath{a^\dagger_{#1}}}
\newcommand{\baseannihilationoperator}[1]{\ensuremath{a^{\vphantom{\dagger}}_{#1}}}

% glossary entry definitions ----------------------------------------------- %

\newglossary*{physics}{Physics}

% dimensions

\newglossaryentry{time-dimension}{%
type=physics,
name=\ensuremath{\mathrm{\mathsf{T}}},
description={time},
sort={dimensiontime},
}

\newglossaryentry{length-dimension}{%
type=physics,
name=\ensuremath{\mathrm{\mathsf{L}}},
description={length},
sort={dimensionlength},
}

\newglossaryentry{mass-dimension}{%
type=physics,
name=\ensuremath{\mathrm{\mathsf{M}}},
description={mass},
sort={dimensionmass},
}

\newglossaryentry{electric-current-dimension}{%
type=physics,
name=\ensuremath{\mathrm{\mathsf{I}}},
description={electric current},
sort={dimensionelectriccurrent},
}

\newglossaryentry{thermodynamic-temperature-dimension}{%
type=physics,
name=\ensuremath{\mathrm{\mathsf{\Theta}}},
description={thermodynamic temperature},
sort={dimensionthermodynamictemperature},
}

\newglossaryentry{amount-of-substance-dimension}{%
type=physics,
name=\ensuremath{\mathrm{\mathsf{N}}},
description={amount of substance},
sort={dimensionamountofsubstance},
}

\newglossaryentry{luminous-intensity-dimension}{%
type=physics,
name=\ensuremath{\mathrm{\mathsf{J}}},
description={luminous intensity},
sort={dimensionluminousintensity},
}

% constants

\newglossaryentry{speed-of-light}{%
type=physics,
name=\ensuremath{c},
description={speed of light \textup{(\metre\usk\reciprocal\second)}},
sort={constantspeedoflight},
}

\newglossaryentry{planck-constant}{%
type=physics,
name=\ensuremath{h},
description={Planck constant \textup{(\joule\usk\second)}},
sort={constantplanck},
}

\newglossaryentry{reduced-planck-constant}{%
type=physics,
name=\ensuremath{\hbar},
description={reduced Planck constant \textup{(\joule\usk\second\usk\reciprocal\radian)}},
sort={constantplanckreduced},
}

\newglossaryentry{elementary-charge}{%
type=physics,
name=\ensuremath{e},
description={elementary charge \textup{(\coulomb)}},
sort={constantelementarycharge},
}

\newglossaryentry{boltzmann-constant}{%
type=physics,
name=\ensuremath{k_B},
description={Boltzmann constant \textup{(\joule\usk\reciprocal\kelvin)}},
sort={constantboltzmann},
}

\newglossaryentry{avogadro-constant}{%
type=physics,
name=\ensuremath{\glsentrytext{number-of-particles}_A},
description={Avogadro constant \textup{(\reciprocal\mole)}},
sort={constantavogadro},
}

\newglossaryentry{vacuum-permeability}{%
type=physics,
name=\ensuremath{\glsentrytext{permeability}_0},
description={vacuum permeability \textup{(\henry\usk\reciprocal\metre)}},
sort={constantvacuumpermeability},
}

\newglossaryentry{vacuum-permittivity}{%
type=physics,
name=\ensuremath{\glsentrytext{permittivity}_0},
description={vacuum permittivity \textup{(\farad\usk\reciprocal\metre)}},
sort={constantvacuumpermittivity},
}

\newglossaryentry{fine-structure-constant}{%
type=physics,
name=\ensuremath{\alpha},
description={Fine-structure constant},
sort={constantfinestructure},
}

\newglossaryentry{gravitational-constant}{%
type=physics,
name=\ensuremath{G},
description={Gravitational constant \textup{(\cubic\meter\usk\reciprocal\kilogram\usk\rpsquare\second)}},
sort={constantgravitational},
}

\newglossaryentry{bohr-radius}{%
type=physics,
name=\ensuremath{a_0},
description={Bohr radius \textup{(\metre)}},
sort={constantradiusbohr},
}

\newglossaryentry{bohr-magneton}{%
type=physics,
name=\ensuremath{\mu_B},
description={Bohr magneton \textup{(\newton\usk\metre\usk\reciprocal\tesla)}},
sort={constantmagnetonbohr},
}

\newglossaryentry{nuclear-magneton}{%
type=physics,
name=\ensuremath{\mu_N},
description={Nuclear magneton \textup{(\newton\usk\metre\usk\reciprocal\tesla)}},
sort={constantmagnetonnuclear},
}

\newglossaryentry{rydberg-constant}{%
type=physics,
name=\ensuremath{R_\infty},
description={Rydberg constant \textup{(\reciprocal\metre)}},
sort={constantrydberg},
}

% quantities (common)

\newglossaryentry{number-of-particles}{%
type=physics,
name=\ensuremath{N},
description={number of particles},
sort={quantitycommonnumberofparticles},
}

\newglossaryentry{mass-density}{%
type=physics,
name=\ensuremath{\rho},
description={mass density \textup{(\kilogram\usk\rpcubic\metre)}},
sort={quantitycommondensitymass},
}

\newglossaryentry{number-density}{%
type=physics,
name=\ensuremath{n},
description={number density \textup{(\rpcubic\meter)}},
sort={quantitycommondensitynumber},
}

\newglossaryentry{momentum}{%
type=physics,
name=\vectorquantity{p},
\veckey=\vectorquantity{p},
\normkey=\ensuremath{p},
\xkey=\ensuremath{p_x},
\ykey=\ensuremath{p_y},
\zkey=\ensuremath{p_z},
description={momentum \textup{(\kilogram\usk\metre\usk\reciprocal\second)}},
sort={quantitycommonmomentumlinear},
}

\newglossaryentry{angular-momentum}{%
type=physics,
name=\vectorquantity{L},
\veckey=\vectorquantity{L},
\normkey=\ensuremath{L},
\xkey=\ensuremath{L_x},
\ykey=\ensuremath{L_y},
\zkey=\ensuremath{L_z},
description={angular momentum \textup{(\kilogram\usk\square\metre\usk\reciprocal\second)}},
sort={quantitycommonmomentumangular},
}

\newglossaryentry{magnetic-moment}{%
type=physics,
name=\vectorquantity{\mu},
\veckey=\vectorquantity{\mu},
\normkey=\ensuremath{\mu},
\xkey=\ensuremath{\mu_x},
\ykey=\ensuremath{\mu_y},
\zkey=\ensuremath{\mu_z},
description={magnetic moment \textup{(\newton\usk\metre\usk\reciprocal\tesla)}},
sort={quantitycommonmomentmagnetic},
}

\newglossaryentry{position}{%
type=physics,
name=\vectorquantity{r},
\veckey=\vectorquantity{r},
\normkey=\ensuremath{r},
\xkey=\ensuremath{x},
\ykey=\ensuremath{y},
\zkey=\ensuremath{z},
description={position \textup{(\meter)}},
sort={quantitycommonposition},
}

\newglossaryentry{polar-angle}{%
type=physics,
name=\ensuremath{\theta},
description={polar angle},
sort={quantitycommonanglepolar},
}

\newglossaryentry{azimuthal-angle}{%
type=physics,
name=\ensuremath{\varphi},
description={azimuthal angle},
sort={quantitycommonangleazimuthal},
}

\newglossaryentry{length}{%
type=physics,
name=\ensuremath{L},
description={length \textup{(\meter)}},
sort={quantitycommonlength},
}

\newglossaryentry{time}{%
type=physics,
name=\ensuremath{t},
description={time \textup{(\second)}},
sort={quantitycommontime},
}

\newglossaryentry{velocity}{%
type=physics,
name=\vectorquantity{v},
\veckey=\vectorquantity{v},
\normkey=\ensuremath{v},
\xkey=\ensuremath{v_x},
\ykey=\ensuremath{v_y},
\zkey=\ensuremath{v_z},
description={velocity \textup{(\metre\usk\reciprocal\second)}},
sort={quantitycommonvelocity},
}

\newglossaryentry{force}{%
type=physics,
name=\vectorquantity{F},
\veckey=\vectorquantity{F},
\normkey=\ensuremath{F},
\xkey=\ensuremath{F_x},
\ykey=\ensuremath{F_y},
\zkey=\ensuremath{F_z},
description={force \textup{(\newton)}},
sort={quantitycommonforce},
}

\newglossaryentry{force-density}{%
type=physics,
name=\vectorquantity{f},
\veckey=\vectorquantity{f},
\normkey=\ensuremath{f},
\xkey=\ensuremath{f_x},
\ykey=\ensuremath{f_y},
\zkey=\ensuremath{f_z},
description={force density \textup{(\newton\usk\rpcubic\metre)}},
sort={quantitycommonforcedensity},
}

\newglossaryentry{body-acceleration}{%
type=physics,
name=\vectorquantity{g},
\veckey=\vectorquantity{g},
\normkey=\ensuremath{g},
\xkey=\ensuremath{g_x},
\ykey=\ensuremath{g_y},
\zkey=\ensuremath{g_z},
description={body acceleration \textup{(\metre\usk\rpsquare\second)}},
sort={quantitycommonbodyacceleration},
}

\newglossaryentry{volume}{%
type=physics,
name=\ensuremath{V},
description={volume \textup{(\cubic\metre)}},
sort={quantitycommonvolume},
}

\newglossaryentry{mass}{%
type=physics,
name=\ensuremath{m},
description={mass \textup{(\kilogram)}},
sort={quantitycommonmass},
}

\newglossaryentry{energy-density}{%
type=physics,
name=\ensuremath{\epsilon},
description={energy density \textup{(\joule\usk\rpcubic\meter)}},
sort={quantitycommonenergydensity},
}

\newglossaryentry{scalar-potential}{%
type=physics,
name=\ensuremath{V},
description={scalar potential \textup{(\joule)}},
sort={quantitycommonscalarpotential},
}

\newglossaryentry{energy}{%
type=physics,
name=\ensuremath{E},
description={energy \textup{(\joule)}},
sort={quantitycommonenergy},
}

% quantities (electromagnetism)

\newglossaryentry{permeability}{%
type=physics,
name=\ensuremath{\mu},
description={permeability \textup{(\henry\usk\reciprocal\metre)}},
sort={quantityelectromagnetismpermeability},
}

\newglossaryentry{permittivity}{%
type=physics,
name=\ensuremath{\varepsilon},
description={permittivity \textup{(\farad\usk\reciprocal\metre)}},
sort={quantityelectromagnetismpermittivity},
}

\newglossaryentry{magnetic-field}{%
type=physics,
name=\vectorquantity{B},
\veckey=\vectorquantity{B},
\normkey=\ensuremath{B},
\xkey=\ensuremath{B_x},
\ykey=\ensuremath{B_y},
\zkey=\ensuremath{B_z},
description={magnetic field \textup{(\tesla)}},
sort={quantityelectromagnetismfieldmagnetic},
}

\newglossaryentry{electric-field}{%
type=physics,
name=\vectorquantity{E},
\veckey=\vectorquantity{E},
\normkey=\ensuremath{E},
\xkey=\ensuremath{E_x},
\ykey=\ensuremath{E_y},
\zkey=\ensuremath{E_z},
description={electric field \textup{(\volt\usk\reciprocal\metre)}},
sort={quantityelectromagnetismfieldelectric},
}

\newglossaryentry{density-charge}{%
type=physics,
name=\ensuremath{\rho_{\glsentrytext{elechg}}},
description={charge density \textup{(\coulomb\usk\rpcubic\metre)}},
sort={quantityelectromagnetismdensitycharge},
}

\newglossaryentry{electric-charge}{%
type=physics,
name=\ensuremath{q},
description={electric charge \textup{(\coulomb)}},
sort={quantityelectromagnetismelectriccharge},
}

\newglossaryentry{electric-conductivity}{%
type=physics,
name=\ensuremath{\sigma},
description={electrical conductivity \textup{(\siemens\usk\reciprocal\metre)}},
sort={quantityelectromagnetismelectricconductivity},
}

\newglossaryentry{density-current}{%
type=physics,
name=\vectorquantity{j},
\veckey=\vectorquantity{j},
\normkey=\ensuremath{j},
\xkey=\ensuremath{j_x},
\ykey=\ensuremath{j_y},
\zkey=\ensuremath{j_z},
description={current density \textup{(\ampere\usk\rpsquare\metre)}},
sort={quantityelectromagnetismdensitycurrent},
}

% quantities (thermodynamics)

\newglossaryentry{pressure}{%
type=physics,
name=\ensuremath{P},
description={pressure \textup{(\pascal)}},
sort={quantitythermodynamicspressure},
}

\newglossaryentry{temperature}{%
type=physics,
name=\ensuremath{T},
description={temperature \textup{(\kelvin)}},
sort={quantitythermodynamicstemperature},
}

\newglossaryentry{internal-energy}{%
type=physics,
name=\ensuremath{U},
description={internal energy \textup{(\joule)}},
sort={quantitythermodynamicsinternalenergy},
}

\newglossaryentry{specific-internal-energy}{%
type=physics,
name=\ensuremath{u},
description={specific internal energy \textup{(\joule\usk\reciprocal\kilogram)}},
sort={quantitythermodynamicsinternalenergyspecific},
}

\newglossaryentry{heat-capacity-ratio}{%
type=physics,
name=\ensuremath{\gamma},
description={heat capacity ratio},
sort={quantitythermodynamicsheatcapacityratio},
}

\newglossaryentry{enthalpy}{%
type=physics,
name=\ensuremath{H},
description={enthalpy \textup{(\joule)}},
sort={quantitythermodynamicsenthalpy},
}

\newglossaryentry{specific-enthalpy}{%
type=physics,
name=\ensuremath{h},
description={specific enthalpy \textup{(\joule\usk\reciprocal\kilogram)}},
sort={quantitythermodynamicsenthalpyspecific},
}

\newglossaryentry{isobaric-heat-capacity}{%
type=physics,
name=\ensuremath{C_{\glsentrytext{pressure}}},
description={isobaric heat capacity \textup{(\joule\usk\reciprocal\kelvin)}},
sort={quantitythermodynamicsheatcapacityisobaric},
}

\newglossaryentry{isochoric-heat-capacity}{%
type=physics,
name=\ensuremath{C_{\glsentrytext{volume}}},
description={isochoric heat capacity \textup{(\joule\usk\reciprocal\kelvin)}},
sort={quantitythermodynamicsheatcapacityisochoric},
}

\newglossaryentry{heat-flux}{%
type=physics,
name=\ensuremath{\phi},
description={heat flux \textup{(\watt\usk\rpsquare\metre)}},
sort={quantitythermodynamicsheatflowdensity},
}

% quantities (plasma)

\newglossaryentry{debye-length}{%
type=physics,
name=\ensuremath{\lambda_D},
description={Debye length \textup{(\meter)}},
sort={quantityplasmadebyelength},
}

\newglossaryentry{alfven-number}{%
type=physics,
name=\ensuremath{A},
description={Alfvén number},
sort={quantityplasmanumberalfven},
}

\newglossaryentry{plasma-beta}{%
type=physics,
name=\ensuremath{\beta},
description={plasma beta},
sort={quantityplasmabeta},
}

\newglossaryentry{debye-number}{%
type=physics,
name=\ensuremath{N_D},
description={Debye number},
sort={quantityplasmanumberdebye},
}

\newglossaryentry{plasma-parameter}{%
type=physics,
name=\ensuremath{\Lambda},
description={plasma parameter},
sort={quantityplasmaparameter},
}

\newglossaryentry{coupling-parameter}{%
type=physics,
name=\ensuremath{\Gamma},
description={coupling parameter},
sort={quantityplasmacouplingparameter},
}

% quantities (fluids)

\newglossaryentry{magnetic-reynolds-number}{%
type=physics,
name=\ensuremath{R_m},
description={magnetic Reynolds number},
sort={quantityfluidsnumberreynolds},
}

\newglossaryentry{mach-number}{%
type=physics,
name=\ensuremath{M},
description={Mach number},
sort={quantityfluidsnumbermach},
}

\newglossaryentry{viscosity-deviatoric-stress}{%
type=physics,
name=\ensuremath{\tau},
description={viscosity deviatoric stress \textup{(\pascal)}},
sort={quantityfluidsviscosity},
}

% quantities (quantum)

\newglossaryentry{wave-function}{%
type=physics,
name=\ensuremath{\psi},
description={wave function},
sort={quantityquantumwavefunction},
}

% quantities (waves)

\newglossaryentry{frequency}{%
type=physics,
name=\ensuremath{\nu},
description={frequency \textup{(\reciprocal\second)}},
sort={quantitywavesfrequency},
}

\newglossaryentry{angular-frequency}{%
type=physics,
name=\ensuremath{\omega},
description={angular frequency \textup{(\radian\usk\reciprocal\second)}},
sort={quantitywavesfrequencyangular},
}

\newglossaryentry{wavelength}{%
type=physics,
name=\ensuremath{\lambda},
description={wavelength \textup{(\metre)}},
sort={quantitywaveswavelength},
}

\newglossaryentry{angular-wavelength}{%
type=physics,
name=\ensuremath{\lambdabar},
description={angular wavelength \textup{(\metre\usk\reciprocal\radian)}},
sort={quantitywaveswavelengthangular},
}

\newglossaryentry{wavenumber}{%
type=physics,
name=\ensuremath{\hat{\nu}},
description={wavenumber \textup{(\reciprocal\metre)}},
sort={quantitywaveswavenumber},
}

\newglossaryentry{angular-wavenumber}{%
type=physics,
name=\vectorquantity{k},
\veckey=\vectorquantity{k},
\normkey=\ensuremath{k},
\xkey=\ensuremath{k_x},
\ykey=\ensuremath{k_y},
\zkey=\ensuremath{k_z},
description={angular wavenumber \textup{(\radian\usk\reciprocal\metre)}},
sort={quantitywaveswavenumberangular},
}

% spaces (quantum)

\newglossaryentry{wave-function-space}{%
type=physics,
name=\ensuremath{\mathscr{F}},
description={wave function space},
sort={spacequantumwavefunctionspace},
}

\newglossaryentry{state-space}{%
type=physics,
name=\ensuremath{\mathscr{E}},
description={state space},
sort={spacequantumstatespace},
}

\newglossaryentry{orbital-state-space}{%
type=physics,
name=\ensuremath{\glsentrytext{state-space}_{\vectorquantity{r}}},
description={state space of a spinless particle moving in a three-dimensional space},
sort={spacequantumstatespaceorbital},
}

\newglossaryentry{sphere-surface-state-space}{%
type=physics,
name=\ensuremath{\glsentrytext{state-space}_\Omega},
description={state space of a spinless particle confined to the surface of a sphere},
sort={spacequantumstatespaceorbitalspheresurface},
}

\newglossaryentry{line-state-space}{%
type=physics,
name=\ensuremath{\glsentrytext{state-space}_x},
\xkey=\ensuremath{\glsentrytext{state-space}_x},
\ykey=\ensuremath{\glsentrytext{state-space}_y},
\zkey=\ensuremath{\glsentrytext{state-space}_z},
description={state space of a spinless particle moving in a one-dimensional space},
sort={spacequantumstatespaceorbitalline},
}

\newglossaryentry{spin-state-space}{%
type=physics,
name=\ensuremath{\glsentrytext{state-space}_s},
description={state space of spin states},
sort={spacequantumstatespacespin},
}

\newglossaryentry{symmetric-state-space}{%
type=physics,
name=\ensuremath{\glsentrytext{state-space}_S},
description={state space of totally symmetric states},
sort={spacequantumstatespaceidenticalsymmetric},
}

\newglossaryentry{antisymmetric-state-space}{%
type=physics,
name=\ensuremath{\glsentrytext{state-space}_A},
description={state space of totally antisymmetric states},
sort={spacequantumstatespaceidenticalantisymmetric},
}

% operators (quantum)

\newglossaryentry{position-operator}{%
type=physics,
name=\vectorquantity{R},
\veckey=\vectorquantity{R},
\normkey=\ensuremath{R},
\xkey=\ensuremath{X},
\ykey=\ensuremath{Y},
\zkey=\ensuremath{Z},
description={position operator \textup{(\meter)}},
sort={operatorquantumposition},
}

\newglossaryentry{momentum-operator}{%
type=physics,
name=\vectorquantity{P},
\veckey=\vectorquantity{P},
\normkey=\ensuremath{P},
\xkey=\ensuremath{P_x},
\ykey=\ensuremath{P_y},
\zkey=\ensuremath{P_z},
description={momentum operator \textup{(\kilogram\usk\metre\usk\reciprocal\second)}},
sort={operatorquantummomentum},
}

\newglossaryentry{kinetic-energy-operator}{%
type=physics,
name=\ensuremath{T},
description={kinetic energy operator \textup{(\joule)}},
sort={operatorquantumenergykinetic},
}

\newglossaryentry{potential-energy-operator}{%
type=physics,
name=\ensuremath{V},
description={potential energy operator \textup{(\joule)}},
sort={operatorquantumenergypotential},
}

\newglossaryentry{hamiltonian-operator}{%
type=physics,
name=\ensuremath{H},
description={Hamiltonian operator \textup{(\joule)}},
sort={operatorquantumenergytotal},
}

\newglossaryentry{angular-momentum-operator}{%
type=physics,
name=\vectorquantity{J},
\veckey=\vectorquantity{J},
\normkey=\ensuremath{J},
\xkey=\ensuremath{J_x},
\ykey=\ensuremath{J_y},
\zkey=\ensuremath{J_z},
description={angular momentum operator \textup{(\kilogram\usk\square\metre\usk\reciprocal\second)}},
sort={operatorquantumangularmomentum},
}

\newglossaryentry{orbital-angular-momentum}{%
type=physics,
name=\vectorquantity{L},
\veckey=\vectorquantity{L},
\normkey=\ensuremath{L},
\xkey=\ensuremath{L_x},
\ykey=\ensuremath{L_y},
\zkey=\ensuremath{L_z},
description={orbital angular momentum operator \textup{(\kilogram\usk\square\metre\usk\reciprocal\second)}},
sort={operatorquantumangularmomentumorbital},
}

\newglossaryentry{spin-angular-momentum}{%
type=physics,
name=\vectorquantity{S},
\veckey=\vectorquantity{S},
\normkey=\ensuremath{S},
\xkey=\ensuremath{S_x},
\ykey=\ensuremath{S_y},
\zkey=\ensuremath{S_z},
description={spin angular momentum operator \textup{(\kilogram\usk\square\metre\usk\reciprocal\second)}},
sort={operatorquantumangularmomentumspin},
}

\newglossaryentry{density-operator}{%
type=physics,
name=\ensuremath{\rho},
description={density operator},
sort={operatorquantumdensity},
}

\newglossaryentry{permutation-operator}{%
type=physics,
name=\basepermutationoperator{\sigma},
description={permutation operator},
sort={operatorquantumpermutation},
}

\newglossaryentry{symmetrizer-operator}{%
type=physics,
name=\ensuremath{S},
description={symmetrizer operator},
sort={operatorquantumsymmetrizer},
}

\newglossaryentry{antisymmetrizer-operator}{%
type=physics,
name=\ensuremath{A},
description={antisymmetrizer operator},
sort={operatorquantumsymmetrizeranti},
}

\newglossaryentry{creation-operator}{%
type=physics,
name=\basecreationoperator{\placeholder{}},
description={creation operator},
sort={operatorquantumladdercreation},
}

\newglossaryentry{annihilation-operator}{%
type=physics,
name=\baseannihilationoperator{\placeholder{}},
description={annihilation operator},
sort={operatorquantumladderannihilation},
}

\newglossaryentry{occupation-number-operator}{%
type=physics,
name=\ensuremath{N},
description={occupation number operator},
sort={operatorquantumoccupationnumber},
}

% elaborate commands (after glossary entry definitions) -------------------- %

% physics
\DeclareRobustCommand{\ux}{\ensuremath{\vectorquantity{u}_\glsx{position}}\xspace} % x base vector
\DeclareRobustCommand{\uy}{\ensuremath{\vectorquantity{u}_\glsy{position}}\xspace} % y base vector
\DeclareRobustCommand{\uz}{\ensuremath{\vectorquantity{u}_\glsz{position}}\xspace} % z base vector
\DeclareRobustCommand{\ur}{\ensuremath{\vectorquantity{u}_\glsnorm{position}}\xspace} % r base vector
\DeclareRobustCommand{\upol}{\ensuremath{\vectorquantity{u}_\gls{polar-angle}}\xspace} % polar base vector
\DeclareRobustCommand{\uazi}{\ensuremath{\vectorquantity{u}_\gls{azimuthal-angle}}\xspace} % azimuthal base vector
\DeclareRobustCommand{\permutationoperator}[1]{\glslink{permutation-operator}{\basepermutationoperator{#1}}}
\DeclareRobustCommand{\creationoperator}[1]{\glslink{creation-operator}{\basecreationoperator{#1}}}
\DeclareRobustCommand{\annihilationoperator}[1]{\glslink{annihilation-operator}{\baseannihilationoperator{#1}}}
