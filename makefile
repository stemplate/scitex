SHELL=/bin/sh

# list of visual elements to be generated
visuals=$(shell cat visuals/targets.txt 2>/dev/null)

# Python interpreter and virtual environment
python=python3
venv:=$(shell cat .venv-path 2>/dev/null || echo ~/.venv/dev/$$(date +'%Y%m%dT%H%M%S'))
requirements=visuals/py/requirements.txt
vpython=. $(venv)/bin/activate && python3

# font for figures
font_url=https://www.gust.org.pl/projects/e-foundry/lm-math/download/latinmodern-math-1959.zip
font_zip=~/.fonts/tmp-lm.zip
font_otf=~/.fonts/latinmodern-math.otf

# prevent the deletion of target files
.PRECIOUS: $(visuals)

# default target
.PHONY: all
all: main.pdf

# create a new virtual environment
$(venv):
ifeq (,$(wildcard $(venv)))
	$(python) -m venv $(venv)
	echo $(venv) > .venv-path
endif

# install Python packages in the virtual environment
.venv-content: $(requirements) $(venv)
	$(vpython) -m pip install --upgrade pip flake8 pylint
	$(vpython) -m pip install --upgrade --requirement $<
	cp $< $@

# set up virtual environment
.PHONY: venv
venv: .venv-content

# install font for figures
.PHONY: font
font:
ifeq (,$(wildcard $(font_otf)))
	mkdir -p ~/.fonts
	curl -o $(font_zip) $(font_url)
	unzip -p $(font_zip) latinmodern-math-1959/otf/latinmodern-math.otf > $(font_otf)
	rm $(font_zip)
endif

# check code (and log file if already compiled)
.PHONY: check
check:
ifneq (,$(shell grep /py/ visuals/targets.txt))
	$(MAKE) venv
	$(vpython) -m pylint visuals/py --rcfile visuals/py/.pylintrc || true
	$(vpython) -m flake8 visuals/py || true
endif
	chktex main
	lacheck main
	grep "LaTeX Warning" output/main.log || true

# delete all files except those that people normally don’t want to recompile
.PHONY: mostlyclean
mostlyclean:
	rm -f -r output
	rm -f -r visuals/py/__pycache__

# delete all files that are normally created by running make
.PHONY: clean
clean: mostlyclean
	rm -f *.pdf
	rm -f visuals/svg/*.pdf
	rm -f visuals/py/*.pdf
	rm -f -r $(venv)
	rm -f .venv-content
	rm -f $(font_otf)

# generate visual elements
.PHONY: visuals
visuals: $(visuals)

# generate a figure from a vector graphic file
visuals/svg/%.pdf: visuals/svg/%.svg | font
	inkscape --export-filename=$@ --export-type=pdf $<

# generate a figure from a Python script
visuals/py/%.pdf: visuals/py/%.py visuals/py/modules/*.py $(requirements) | font venv
	$(vpython) $< $@

# generate a document
%.pdf: %.tex $(shell find preamble contents -type f) $(visuals)
	for subdir in $(shell find contents -type d); do mkdir -p output/$$subdir; done
	pdflatex -output-directory output $<
	biber output/$*
	makeindex output/$*
	pdflatex -output-directory output $<
	biber output/$*
	makeindex output/$*
	pdflatex -output-directory output $<
	mv output/*.pdf .
