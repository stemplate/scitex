# SciTex

> What does it mean?

The name "SciTex" comes from the contraction of "Science" and "Tex".

> What is it for?

SciTex is a template for LaTeX scientific documents.

> Is it hard to use?

You'll be fine as long as you know a little about LaTeX.

## Background

When starting a new LaTeX document, difficulties may arise.
A template can save time by providing a pre-designed foundation with predefined structure and formatting.
This allows users to focus on content creation rather than spending time on manual setup.

## Features

This project offers a LaTeX template for scientific documents with the following benefits and features:

* Efficient organization of the work directory.
* A single command to compile the document: `make`.
* A single command to clean the directory: `make clean`.
* A single command to check code quality: `make check`.
* A guide to good practice.
* Harmonious use of popular scientific writing packages.
* A pre-filled glossary of mathematical symbols and physical quantities.
* Continuous integration with GitLab.

The [makefile](/makefile) is designed so that each time you compile with `make`, the visual elements are regenerated if the Python scripts or SVG files that produce them have been updated.

Continuous integration enables automatic online publication of the document.
The PDF file generated by the project can be consulted [here](https://stemplate.gitlab.io/scitex/main.pdf).

## Installation

The project is designed for a POSIX system.
To compile the document, the project requires the following programs:

* [TeX Live](https://tug.org/texlive/)
* [GNU make](https://www.gnu.org/software/make)
* [Inkscape](https://inkscape.org) (if you wish to incorporate vector graphics)
* [Python](https://www.python.org/) (if you wish to incorporate script-generated visual elements)

The Python packages needed to generate the visual elements are listed in [visuals/py/requirements.txt](/visuals/py/requirements.txt).
If you need additional packages, add them to this file.
It is always recommend installing Python packages in a [virtual environment](https://docs.python.org/3/library/venv.html).
The [makefile](/makefile) creates a virtual environment for you and installs the required packages.

## Usage

The best practice guide and code examples can be found in file [contents/complements.tex](/contents/complements.tex).
Also visible in the compiled file [here](https://stemplate.gitlab.io/scitex/main.pdf#appendix.A).

After a push on the main branch and a successful compilation on the GitLab servers, the PDF is published at this address: [https://stemplate.gitlab.io/scitex/main.pdf](https://stemplate.gitlab.io/scitex/main.pdf).
If the project is to remain private, remove the "pages" job in [.gitlab-ci.yml](.gitlab-ci.yml).
The PDF will then no longer be published.

If the document is not supposed to contain visual elements, the [visuals](/visuals) directory can be safely deleted.
Be sure to remove any visual element inclusions in file [contents/complements.tex](/contents/complements.tex).

To add a visual element generated from a script or vector drawing file, simply add a .svg or .py file in [visuals/svg](/visuals/svg) or [visuals/py](/visuals/py), and also add the path to the file to be generated in [visuals/targets.txt](/visuals/targets.txt).
The latter file is read by the makefile to determine which visual element need to be generated.

## Credits

* Dunstan Becht

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
