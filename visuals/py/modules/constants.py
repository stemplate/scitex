# -*- coding: utf-8 -*-

"""Physical constants.

"""

import math


NAME = {
    'hbar': "reduced Planck constant",
    'eps0': "vacuum electric permittivity",
    'm_e': "electron mass",
    'e': "elementary charge",
    'a_0': "Bohr radius",
}

SI = {
    'hbar': 6.626_070_15e-34/(2*math.pi),  # (J.s)
    'eps0': 8.854_187_812_8e-12,  # (F/m)
    'm_e': 9.109_383_701_5e-31,  # (kg)
    'e': 1.602_176_634e-19,  # (C)
    'a_0': 5.291_772_109_03e-11,  # (m)
}
