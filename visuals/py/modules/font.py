# -*- coding: utf-8 -*-

"""This module can be used to set the font used by the matplotlib module.

"""

import os

import matplotlib.pyplot as plt
from matplotlib import font_manager


def use_latinmodern(size=10):
    """Use Latin Modern Math font in matplotlib.

    Parameters
    ----------
    size : int
        Font size (pt).

    """
    font_path = os.path.expanduser('~/.fonts/latinmodern-math.otf')
    font_manager.fontManager.addfont(str(font_path))
    prop = font_manager.FontProperties(fname=font_path)
    plt.rcParams["font.family"] = prop.get_name()
    plt.rcParams["mathtext.fontset"] = "cm"
    plt.rcParams["font.size"] = size
    plt.rcParams["legend.fontsize"] = size
    plt.rcParams["axes.titlesize"] = size
    plt.rcParams["figure.titlesize"] = size


if __name__ == "__main__":
    use_latinmodern()
    plt.figure()
    plt.subplot(projection="mollweide")
    plt.title("Title example")
    plt.grid(True)
    plt.savefig('font.pdf')
