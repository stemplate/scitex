#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Script to generate a figure.

"""

import sys
from cycler import cycler

import numpy as np
import matplotlib.pyplot as plt

from modules import font


__author__ = 'Author'

INCH = 25.4  # mm/inch
WIDTH = 170  # mm
HEIGHT = 100  # mm
FIGSIZE = (WIDTH/INCH, HEIGHT/INCH)  # inch
METADATA = {
    'Title': 'Figure title',
    'Author': __author__,
}
CYCLER = (
    cycler(marker=['', '.', '*']) *
    cycler(linestyle=['-', '--', ':', '-.'])
)


def export(path):
    """Exports the figure to the requested path.

    Parameters
    ----------
    path : str
        Path where figure is exported.

    """
    font.use_latinmodern()
    variable = np.linspace(0, 10*np.pi, 300)
    cos = np.cos(variable)
    sin = np.sin(variable)
    fig, axes = plt.subplots(figsize=FIGSIZE)
    axes.set_title('A single plot')
    axes.set_prop_cycle(CYCLER)
    axes.plot(variable, cos, 'k', label=r"$\cos(x)$")
    axes.plot(variable, sin, 'k:', label=r"$\sin(x)$")
    axes.legend()
    axes.set_xlabel(r"$x$")
    axes.set_ylabel(r"$y$")
    axes.set_ylim((-1.5, 1.5))
    axes.grid()
    fig.savefig(path, metadata=METADATA)


if __name__ == '__main__':

    export(sys.argv[1])
